## Go Blog - Skill Test 

Este proyecto skill test con golang, implementa el API base de:

https://jsonplaceholder.typicode.com

Con endpoints para User, Posts, Comments, Photos

Para la implementacion se utilizo las siguientes libs:

go-gin -> API REST Server  
gorm -> ORM para SQLite y PostgreSQL
bcrypt -> Cifrado Hash Bcrypt

# Como ejecutar el proyecto ?

git clone gitlab.com/danielrb/go-blog

docker compose up 

curl -ivh http://localhost:8080/api/v1/user

Si se desea utilizar SQLite, modificar el archivo Dockerfile y cambiar la linea a:

ENV APP_ENVIRONMENT DEV

# Carga de Datos
Para la carga de datos de prueba ejecutar:

http://localhost:8080/api/v1/load

# Login  
Para login a la aplicacion:

curl -iv -X POST -H "Content-Type: application/json" http://localhost:8080/api/v1/login -d `{"passw": "abc123","email": "Nathan@yesenia.net"}`

Si las credenciales son correctas, se retorna un header con el token generado de autenticacion

x-auth-key 8949jcjdjisiji3

# Logout
http://localhost:8080/api/v1/logout

# Endpoints Implementados
Para todas las operaciones se debe enviar el token de autenticacion en forma de header 

x-auth-key 8949jcjdjisiji3

http://localhost:8080/api/v1/user
http://localhost:8080/api/v1/post
http://localhost:8080/api/v1/comment
http://localhost:8080/api/v1/photo

# Operaciones CRUD (Consultar, Modificar, Eliminar) por ID
http://localhost:8080/api/v1/{endpoint}/1
