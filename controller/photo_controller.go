package controller

import (
	"net/http"
	"strconv"

	"gitlab.com/danielrb/go-blog/model"
	"gitlab.com/danielrb/go-blog/service"

	"github.com/gin-gonic/gin"
)

type PhotoController interface {
	NewPhoto(c *gin.Context)
	GetAllPhotos(c *gin.Context)
	GetPhotoById(c *gin.Context)
	UpdatePhoto(c *gin.Context)
	DeletePhoto(c *gin.Context)
	LoadPhotos(c *gin.Context)
}

type PhotoControllerImpl struct {
	svc service.PhotoService
}

func (u PhotoControllerImpl) NewPhoto(c *gin.Context) {

	var mo model.Photo

	if err := c.ShouldBindJSON(&mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "bad json request"})
		return
	}

	if err := u.svc.NewPhoto(mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "new Photo created"})
}

func (u PhotoControllerImpl) GetAllPhotos(c *gin.Context) {

	c.JSON(http.StatusOK, u.svc.GetAllPhotos())

}

func (u PhotoControllerImpl) GetPhotoById(c *gin.Context) {
	var id int
	var err error
	var mo model.Photo

	if id, err = strconv.Atoi(c.Param("photoID")); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Bad ID"})
		return
	}

	if mo, err = u.svc.GetPhotoById(uint(id)); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Photo not found"})
		return
	}

	c.JSON(http.StatusOK, mo)
}

func (u PhotoControllerImpl) UpdatePhoto(c *gin.Context) {
	var mo model.Photo
	var id int
	var err error

	if err := c.ShouldBindJSON(&mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "bad json request"})
		return
	}

	if id, err = strconv.Atoi(c.Param("photoID")); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Bad ID"})
		return
	}

	mo.ID = uint(id)

	if err := u.svc.UpdatePhoto(mo); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, mo)
}

func (u PhotoControllerImpl) DeletePhoto(c *gin.Context) {
	var mo model.Photo
	var id int
	var err error

	if err := c.ShouldBindJSON(&mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "bad json request"})
		return
	}

	if id, err = strconv.Atoi(c.Param("photoID")); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Bad ID"})
		return
	}

	mo.ID = uint(id)

	if err := u.svc.DeletePhoto(mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, mo)
}

func PhotoControllerInit(constr string) *PhotoControllerImpl {

	db := service.Init(constr)

	return &PhotoControllerImpl{
		svc: service.PhotoServiceInit(db),
	}
}
