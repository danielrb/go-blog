package controller

import (
	"net/http"
	"strconv"

	"gitlab.com/danielrb/go-blog/model"
	"gitlab.com/danielrb/go-blog/service"

	"github.com/gin-gonic/gin"
)

type CommentController interface {
	NewComment(c *gin.Context)
	GetAllComments(c *gin.Context)
	GetCommentById(c *gin.Context)
	UpdateComment(c *gin.Context)
	DeleteComment(c *gin.Context)
	LoadComments(c *gin.Context)
}

type CommentControllerImpl struct {
	svc service.CommentService
}

func (u CommentControllerImpl) NewComment(c *gin.Context) {

	var mo model.Comment

	if err := c.ShouldBindJSON(&mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "bad json request"})
		return
	}

	if err := u.svc.NewComment(mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "new Comment created"})
}

func (u CommentControllerImpl) GetAllComments(c *gin.Context) {

	c.JSON(http.StatusOK, u.svc.GetAllComments())

}

func (u CommentControllerImpl) GetCommentById(c *gin.Context) {
	var id int
	var err error
	var mo model.Comment

	if id, err = strconv.Atoi(c.Param("commentID")); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Bad ID"})
		return
	}

	if mo, err = u.svc.GetCommentById(uint(id)); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Comment not found"})
		return
	}

	c.JSON(http.StatusOK, mo)
}

func (u CommentControllerImpl) UpdateComment(c *gin.Context) {
	var mo model.Comment
	var id int
	var err error

	if err := c.ShouldBindJSON(&mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "bad json request"})
		return
	}

	if id, err = strconv.Atoi(c.Param("commentID")); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Bad ID"})
		return
	}

	mo.ID = uint(id)

	if err := u.svc.UpdateComment(mo); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, mo)
}

func (u CommentControllerImpl) DeleteComment(c *gin.Context) {
	var mo model.Comment
	var id int
	var err error

	if err := c.ShouldBindJSON(&mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "bad json request"})
		return
	}

	if id, err = strconv.Atoi(c.Param("commentID")); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Bad ID"})
		return
	}

	mo.ID = uint(id)

	if err := u.svc.DeleteComment(mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, mo)
}

func CommentControllerInit(constr string) *CommentControllerImpl {

	db := service.Init(constr)

	return &CommentControllerImpl{
		svc: service.CommentServiceInit(db),
	}
}
