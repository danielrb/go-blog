package controller

import (
	"net/http"
	"strconv"

	"gitlab.com/danielrb/go-blog/model"
	"gitlab.com/danielrb/go-blog/service"

	"github.com/gin-gonic/gin"
)

type UserController interface {
	NewUser(c *gin.Context)
	GetAllUsers(c *gin.Context)
	GetUserById(c *gin.Context)
	UpdateUser(c *gin.Context)
	DeleteUser(c *gin.Context)
	LoadUsers(c *gin.Context)
}

type UserControllerImpl struct {
	svc service.UserService
}

func (u UserControllerImpl) NewUser(c *gin.Context) {

	var mo model.User

	if err := c.ShouldBindJSON(&mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "bad json request"})
		return
	}

	if err := u.svc.NewUser(mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "new user created"})
}

func (u UserControllerImpl) GetAllUsers(c *gin.Context) {

	c.JSON(http.StatusOK, u.svc.GetAllUsers())

}

func (u UserControllerImpl) GetUserById(c *gin.Context) {
	var id int
	var err error
	var mo model.User

	if id, err = strconv.Atoi(c.Param("userID")); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Bad ID"})
		return
	}

	if mo, err = u.svc.GetUserById(uint(id)); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "User not found"})
		return
	}

	c.JSON(http.StatusOK, mo)
}

func (u UserControllerImpl) UpdateUser(c *gin.Context) {
	var mo model.User
	var id int
	var err error

	if err := c.ShouldBindJSON(&mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "bad json request"})
		return
	}

	if id, err = strconv.Atoi(c.Param("userID")); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Bad ID"})
		return
	}

	mo.ID = uint(id)

	if err := u.svc.UpdateUser(mo); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, mo)
}

func (u UserControllerImpl) DeleteUser(c *gin.Context) {
	var mo model.User
	var id int
	var err error

	if err := c.ShouldBindJSON(&mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "bad json request"})
		return
	}

	if id, err = strconv.Atoi(c.Param("userID")); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Bad ID"})
		return
	}

	mo.ID = uint(id)

	if err := u.svc.DeleteUser(mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, mo)
}

func UserControllerInit(constr string) *UserControllerImpl {

	db := service.Init(constr)

	return &UserControllerImpl{
		svc: service.UserServiceInit(db),
	}
}
