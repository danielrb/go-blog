package controller

import (
	"encoding/base64"
	"net/http"
	"os"

	"gitlab.com/danielrb/go-blog/model"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type LoginController interface {
	Login(c *gin.Context)
}

type LoginControllerImpl struct{}

func (u LoginControllerImpl) Login(c *gin.Context) {

	var mo model.Login

	if err := c.ShouldBindJSON(&mo); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "bad json request"})
		return
	}

	if mo.Passw == os.Getenv("APP_TOKEN") {
		token := GeneratePassword(mo.Passw)

		c.Header("x-auth-key", token)
		os.Setenv("x-auth-key", token)

		c.AbortWithStatusJSON(http.StatusOK, gin.H{"message": "accepted"})
		return
	}

	c.JSON(http.StatusUnauthorized, gin.H{"message": "Bad credentials"})
}

func (u LoginControllerImpl) Logout(c *gin.Context) {

	os.Setenv("x-auth-key", "-")

	c.JSON(http.StatusUnauthorized, gin.H{"message": "Logged out"})
}

func GeneratePassword(passw string) string {

	hash, _ := bcrypt.GenerateFromPassword([]byte(passw), bcrypt.DefaultCost)

	return base64.StdEncoding.EncodeToString(hash)
}

func LoginControllerInit() *LoginControllerImpl {
	return &LoginControllerImpl{}
}
