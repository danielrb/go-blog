package controller

import (
	"net/http"
	"strconv"

	"gitlab.com/danielrb/go-blog/model"
	"gitlab.com/danielrb/go-blog/service"

	"github.com/gin-gonic/gin"
)

type PostController interface {
	NewPost(c *gin.Context)
	GetAllPosts(c *gin.Context)
	GetPostById(c *gin.Context)
	UpdatePost(c *gin.Context)
	DeletePost(c *gin.Context)
	LoadPosts(c *gin.Context)
}

type PostControllerImpl struct {
	svc service.PostService
}

func (u PostControllerImpl) NewPost(c *gin.Context) {

	var mo model.Post

	if err := c.ShouldBindJSON(&mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "bad json request"})
		return
	}

	if err := u.svc.NewPost(mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "new Post created"})
}

func (u PostControllerImpl) GetAllPosts(c *gin.Context) {

	c.JSON(http.StatusOK, u.svc.GetAllPosts())

}

func (u PostControllerImpl) GetPostById(c *gin.Context) {
	var id int
	var err error
	var mo model.Post

	if id, err = strconv.Atoi(c.Param("postID")); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Bad ID"})
		return
	}

	if mo, err = u.svc.GetPostById(uint(id)); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Post not found"})
		return
	}

	c.JSON(http.StatusOK, mo)
}

func (u PostControllerImpl) UpdatePost(c *gin.Context) {
	var mo model.Post
	var id int
	var err error

	if err := c.ShouldBindJSON(&mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "bad json request"})
		return
	}

	if id, err = strconv.Atoi(c.Param("postID")); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Bad ID"})
		return
	}

	mo.ID = uint(id)

	if err := u.svc.UpdatePost(mo); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, mo)
}

func (u PostControllerImpl) DeletePost(c *gin.Context) {
	var mo model.Post
	var id int
	var err error

	if err := c.ShouldBindJSON(&mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "bad json request"})
		return
	}

	if id, err = strconv.Atoi(c.Param("postID")); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Bad ID"})
		return
	}

	mo.ID = uint(id)

	if err := u.svc.DeletePost(mo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, mo)
}

func PostControllerInit(constr string) *PostControllerImpl {

	db := service.Init(constr)

	return &PostControllerImpl{
		svc: service.PostServiceInit(db),
	}
}
