package controller

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"sync"
	"time"

	"gitlab.com/danielrb/go-blog/model"
	"gitlab.com/danielrb/go-blog/service"

	"github.com/gin-gonic/gin"
)

type LoadController interface {
	Load(c *gin.Context)
}

type LoadControllerImpl struct {
	userSvc    service.UserService
	postSvc    service.PostService
	photoSvc   service.PhotoService
	commentSvc service.CommentService
}

func (u LoadControllerImpl) Load(c *gin.Context) {

	if err := u.LoadUsers(); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Cannot load Users " + err.Error()})
		return
	}

	if err := u.LoadPosts(); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Cannot load Posts " + err.Error()})
		return
	}

	if err := u.LoadComments(); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Cannot load Comments " + err.Error()})
		return
	}

	if err := u.LoadPhotos(); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Cannot load Photos " + err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "OK"})
}

func LoadControllerInit(constr string) *LoadControllerImpl {
	db := service.Init(constr)

	return &LoadControllerImpl{
		userSvc:    service.UserServiceInit(db),
		postSvc:    service.PostServiceInit(db),
		photoSvc:   service.PhotoServiceInit(db),
		commentSvc: service.CommentServiceInit(db),
	}
}

func (u LoadControllerImpl) LoadUsers() error {

	client := http.Client{
		Timeout: 5 * time.Second,
	}

	resp, err := client.Get("https://jsonplaceholder.typicode.com/users")

	if err != nil {
		return err
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	var users []model.User

	if err = json.Unmarshal(body, &users); err != nil {
		return err
	}

	var wg sync.WaitGroup

	for _, v := range users {
		wg.Add(1)

		go func() {
			defer wg.Done()

			if err := u.userSvc.NewUser(v); err != nil {
				log.Printf("Cannot create User %d - %s \n", v.ID, v.Name)
			}
		}()
	}
	wg.Wait()

	return nil
}

func (u LoadControllerImpl) LoadPosts() error {

	client := http.Client{
		Timeout: 30 * time.Second,
	}

	resp, err := client.Get("https://jsonplaceholder.typicode.com/posts")

	if err != nil {
		return err
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	var Posts []model.Post

	if err = json.Unmarshal(body, &Posts); err != nil {
		return err
	}

	log.Printf("Total Posts %d\n", len(Posts))

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		defer wg.Done()

		if err := u.postSvc.BatchInsert(Posts); err != nil {
			log.Printf("Cannot create Post %s \n", err.Error())
		}
	}()

	wg.Wait()

	return nil
}

func (u LoadControllerImpl) LoadPhotos() error {

	client := http.Client{
		Timeout: 30 * time.Second,
	}

	resp, err := client.Get("https://jsonplaceholder.typicode.com/photos")

	if err != nil {
		return err
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	var Photos []model.Photo

	if err = json.Unmarshal(body, &Photos); err != nil {
		return err
	}

	log.Printf("Total Photos %d\n", len(Photos))

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		defer wg.Done()

		if err := u.photoSvc.BatchInsert(Photos); err != nil {
			log.Printf("Cannot create Photo %s \n", "")
		}
	}()

	wg.Wait()

	return nil
}

func (u LoadControllerImpl) LoadComments() error {

	client := http.Client{
		Timeout: 5 * time.Second,
	}

	resp, err := client.Get("https://jsonplaceholder.typicode.com/comments")

	if err != nil {
		return err
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	var Comments []model.Comment

	if err = json.Unmarshal(body, &Comments); err != nil {
		return err
	}

	log.Printf("Total Comments %d\n", len(Comments))

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		defer wg.Done()

		if err := u.commentSvc.BatchInsert(Comments); err != nil {
			log.Printf("Cannot create Comment %s \n", err.Error())
		}
	}()

	wg.Wait()

	return nil
}
