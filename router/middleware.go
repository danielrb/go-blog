package router

import (
	"encoding/base64"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func AuthMiddleware() gin.HandlerFunc {

	return func(c *gin.Context) {

		api_token := os.Getenv("x-auth-key")

		token := c.GetHeader("x-auth-key")

		log.Printf("Header: %s Server: %s", token, api_token)

		if token == "" {
			c.AbortWithStatus(http.StatusBadRequest)
		}
		if token != api_token {
			c.AbortWithStatus(http.StatusUnauthorized)
		}

		c.Next()
	}
}

func CheckPasswordHash(passw, hash string) bool {

	bin, _ := base64.StdEncoding.DecodeString(passw)

	err := bcrypt.CompareHashAndPassword([]byte(hash), bin)
	return err == nil
}
