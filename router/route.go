package router

import (
	"gitlab.com/danielrb/go-blog/controller"

	"github.com/gin-gonic/gin"
)

func Init() *gin.Engine {
	return gin.Default()
}

func RegisterRoutes(router *gin.Engine, constr string) {

	loginCtl := controller.LoginControllerInit()
	userCtl := controller.UserControllerInit(constr)
	photoCtl := controller.PhotoControllerInit(constr)
	commentCtl := controller.CommentControllerInit(constr)
	postCtl := controller.PostControllerInit(constr)
	loadCtl := controller.LoadControllerInit(constr)

	api := router.Group("/api/v1")
	{
		api.POST("/login", loginCtl.Login)
		api.GET("/logout", loginCtl.Logout)

		api.GET("/load", loadCtl.Load)

		//api.Use(AuthMiddleware())

		user := api.Group("/user")
		user.GET("", userCtl.GetAllUsers)
		user.POST("", userCtl.NewUser)
		user.GET("/:userID", userCtl.GetUserById)
		user.PUT("/:userID", userCtl.UpdateUser)
		user.DELETE("/:userID", userCtl.DeleteUser)

		photo := api.Group("/photo")
		photo.GET("", photoCtl.GetAllPhotos)
		photo.POST("", photoCtl.NewPhoto)
		photo.GET("/:photoID", photoCtl.GetPhotoById)
		photo.PUT("/:photoID", photoCtl.UpdatePhoto)
		photo.DELETE("/:photoID", photoCtl.DeletePhoto)

		comment := api.Group("/comment")
		comment.GET("", commentCtl.GetAllComments)
		comment.POST("", commentCtl.NewComment)
		comment.GET("/:commentID", commentCtl.GetCommentById)
		comment.PUT("/:commentID", commentCtl.UpdateComment)
		comment.DELETE("/:commentID", commentCtl.DeleteComment)

		post := api.Group("/post")
		post.GET("", postCtl.GetAllPosts)
		post.POST("", postCtl.NewPost)
		post.GET("/:postID", postCtl.GetPostById)
		post.PUT("/:postID", postCtl.UpdatePost)
		post.DELETE("/:postID", postCtl.DeletePost)
	}

	/*
		router.NoRoute(func(c *gin.Context) {
			c.JSON(http.StatusNotFound, gin.H{"message": "404 page not found"})
		})

		router.NoMethod(func(c *gin.Context) {
			c.JSON(http.StatusMethodNotAllowed, gin.H{"message": "405 method not allowed"})
		})
	*/
}
