# Stage 
FROM golang:1.22.2-bullseye AS builder

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . ./
RUN ls -la ./

# Build
RUN CGO_ENABLED=1 GOOS=linux go build -o go-blog

FROM debian:bullseye-slim AS runtime

WORKDIR /app

RUN set -x && apt-get update -y \
    && apt-get install -y -qq --no-install-recommends openssl ca-certificates \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /app/go-blog/go-blog go-blog

ENV APP_ENVIRONMENT PROD
ENV APP_TOKEN abc123
ENV SERVER_PORT :8080
ENV DB_CONSTR postgres:passw@localhost:5432/postgres

EXPOSE 8080

CMD ["./go-blog"]
