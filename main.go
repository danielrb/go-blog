package main

import (
	"gitlab.com/danielrb/go-blog/config"
	"gitlab.com/danielrb/go-blog/router"

	"log"
)

func main() {

	conf := config.LoadConfig()

	log.Printf("AppVersion: %s - Listening on %s \n", conf.AppVersion, conf.ServerPort)

	app := router.Init()

	router.RegisterRoutes(app, conf.DBConstr)

	app.Run(conf.ServerPort)

}
