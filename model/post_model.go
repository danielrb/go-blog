package model

import (
	"time"
)

type Post struct {
	ID        uint      `gorm:"primaryKey" json:"id"`
	UserID    int       `json:"userId"`
	Title     string    `json:"title"`
	Body      string    `json:"body"`
	State     int       `json:"-" gorm:"default:1"`
	CreatedAt time.Time `json:"-" gorm:"autoCreateTime:milli"`
	UpdatedAt time.Time `json:"-" gorm:"autoUpdateTime:milli"`
}
