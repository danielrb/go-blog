package model

import (
	"time"
)

type Photo struct {
	ID        uint      `gorm:"primaryKey" json:"id"`
	AlbumID   int       `json:"albumId"`
	Title     string    `json:"title"`
	Url       string    `json:"url"`
	Thumbnail string    `json:"thumbnailUrl"`
	State     int       `json:"-" gorm:"default:1"`
	CreatedAt time.Time `json:"-" gorm:"autoCreateTime:milli"`
	UpdatedAt time.Time `json:"-" gorm:"autoUpdateTime:milli"`
}
