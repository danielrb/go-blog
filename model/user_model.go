package model

import "time"

type User struct {
	ID       uint   `gorm:"primaryKey" json:"id"`
	Name     string `json:"name"`
	UserName string `json:"username"`
	Email    string `json:"email"`
	Address  struct {
		Street  string `json:"street"`
		Suite   string `json:"suite"`
		City    string `json:"city"`
		Zipcode string `json:"zipcode"`
		Geo     struct {
			Lat string `json:"lat"`
			Lng string `json:"lng"`
		} `gorm:"embedded" json:"geo"`
	} `gorm:"embedded" json:"address"`
	Phone   string `json:"phone"`
	Website string `json:"website"`
	Company struct {
		CompanyName string `json:"name"`
		CatchPhrase string `json:"catchPhrase"`
		Bs          string `json:"bs"`
	} `gorm:"embedded" json:"company"`
	State     int       `json:"-" gorm:"default:1"`
	CreatedAt time.Time `json:"-" gorm:"autoCreateTime:milli"`
	UpdatedAt time.Time `json:"-" gorm:"autoUpdateTime:milli"`
}
