package model

import (
	"time"
)

type Login struct {
	ID        uint      `gorm:"primaryKey" json:"-"`
	Email     string    `json:"email"`
	Passw     string    `json:"passw"`
	State     int       `json:"-" gorm:"default:1"`
	CreatedAt time.Time `json:"-" gorm:"autoCreateTime:milli"`
	UpdatedAt time.Time `json:"-" gorm:"autoUpdateTime:milli"`
}
