package config

import (
	"fmt"
	"os"
)

type AppConfig struct {
	AppVersion     string
	AppEnvironment string
	AppToken       string
	DBConstr       string
	ServerPort     string
	Metadata       map[string]string
}

func LoadConfig() AppConfig {
	var conf AppConfig

	conf.AppVersion = "1.0.0"
	conf.AppToken = os.Getenv("APP_TOKEN")
	conf.AppEnvironment = os.Getenv("APP_ENVIRONMENT")
	conf.ServerPort = os.Getenv("SERVER_PORT")

	conf.DBConstr = fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=America/La_Paz",
		os.Getenv("POSTGRES_SERVER"),
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_DB"),
		os.Getenv("POSTGRES_PORT"))
	return conf
}
