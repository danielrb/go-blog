package service

import (
	"fmt"
	"log"

	"gitlab.com/danielrb/go-blog/model"

	"gorm.io/gorm"
)

type UserService interface {
	NewUser(mo model.User) error
	GetAllUsers() []model.User
	GetUserById(id uint) (model.User, error)
	UpdateUser(mo model.User) error
	DeleteUser(mo model.User) error
}

type UserServiceImpl struct {
	db *gorm.DB
}

func (u UserServiceImpl) NewUser(mo model.User) error {

	result := u.db.Create(&mo)

	return result.Error
}

func (u UserServiceImpl) GetAllUsers() []model.User {

	var users []model.User

	result := u.db.Find(&users)

	log.Printf("Rows affected %d", result.RowsAffected)

	return users
}

func (u UserServiceImpl) GetUserById(id uint) (model.User, error) {
	var user model.User

	result := u.db.Find(&user, id)

	if result.RowsAffected == 0 {
		return user, fmt.Errorf("ID %d not found", id)
	}

	return user, result.Error
}

func (u UserServiceImpl) UpdateUser(mo model.User) error {
	var user model.User

	result := u.db.First(&user, mo.ID)

	if result.RowsAffected == 0 {
		return fmt.Errorf("ID %d not found on DB", mo.ID)
	}

	mo.CreatedAt = user.CreatedAt
	mo.State = 2

	result = u.db.Save(&mo)

	if result.RowsAffected == 0 {
		return result.Error
	}

	return result.Error
}

func (u UserServiceImpl) DeleteUser(mo model.User) error {

	result := u.db.Delete(&mo)

	if result.RowsAffected == 0 {
		return fmt.Errorf("ID %d not found", mo.ID)
	}

	return result.Error
}

func UserServiceInit(d *gorm.DB) *UserServiceImpl {
	return &UserServiceImpl{
		db: d,
	}
}
