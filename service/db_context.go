package service

import (
	"log"
	"os"

	"gitlab.com/danielrb/go-blog/model"

	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var DB *gorm.DB

func Init(constr string) *gorm.DB {
	var err error

	env := os.Getenv("APP_ENVIRONMENT")

	switch env {
	case "DEV":
		{
			DB, err = gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
		}

	case "PROD":
		{
			DB, err = gorm.Open(postgres.Open(constr), &gorm.Config{})
		}

	default:
		break
	}

	if err != nil {
		log.Fatal("Cannot connect to database")
	}

	DB.AutoMigrate(&model.User{}, &model.Photo{}, &model.Comment{}, &model.Post{})

	return DB
}
