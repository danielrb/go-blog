package service

import (
	"fmt"
	"log"

	"gitlab.com/danielrb/go-blog/model"

	"gorm.io/gorm"
)

type CommentService interface {
	NewComment(mo model.Comment) error
	BatchInsert(lmo []model.Comment) error
	GetAllComments() []model.Comment
	GetCommentById(id uint) (model.Comment, error)
	UpdateComment(mo model.Comment) error
	DeleteComment(mo model.Comment) error
}

type CommentServiceImpl struct {
	db *gorm.DB
}

func (u CommentServiceImpl) NewComment(mo model.Comment) error {

	result := u.db.Create(&mo)

	return result.Error
}

func (u CommentServiceImpl) BatchInsert(lmo []model.Comment) error {

	result := u.db.CreateInBatches(&lmo, len(lmo))

	log.Printf("Total Rows: %d\n", result.RowsAffected)

	return result.Error
}

func (u CommentServiceImpl) GetAllComments() []model.Comment {

	var Comments []model.Comment

	result := u.db.Find(&Comments)

	log.Printf("Rows affected %d", result.RowsAffected)

	return Comments
}

func (u CommentServiceImpl) GetCommentById(id uint) (model.Comment, error) {
	var Comment model.Comment

	result := u.db.Find(&Comment, id)

	if result.RowsAffected == 0 {
		return Comment, fmt.Errorf("ID %d not found", id)
	}

	return Comment, result.Error
}

func (u CommentServiceImpl) UpdateComment(mo model.Comment) error {
	var Comment model.Comment

	result := u.db.First(&Comment, mo.ID)

	if result.RowsAffected == 0 {
		return fmt.Errorf("ID %d not found on DB", mo.ID)
	}

	mo.CreatedAt = Comment.CreatedAt
	mo.State = 2

	result = u.db.Save(&mo)

	if result.RowsAffected == 0 {
		return result.Error
	}

	return result.Error
}

func (u CommentServiceImpl) DeleteComment(mo model.Comment) error {

	result := u.db.Delete(&mo)

	if result.RowsAffected == 0 {
		return fmt.Errorf("ID %d not found", mo.ID)
	}

	return result.Error
}

func CommentServiceInit(d *gorm.DB) *CommentServiceImpl {
	return &CommentServiceImpl{
		db: d,
	}
}
