package service

import (
	"fmt"
	"log"

	"gitlab.com/danielrb/go-blog/model"

	"gorm.io/gorm"
)

type PostService interface {
	NewPost(mo model.Post) error
	BatchInsert(lmo []model.Post) error
	GetAllPosts() []model.Post
	GetPostById(id uint) (model.Post, error)
	UpdatePost(mo model.Post) error
	DeletePost(mo model.Post) error
}

type PostServiceImpl struct {
	db *gorm.DB
}

func (u PostServiceImpl) NewPost(mo model.Post) error {

	result := u.db.Create(&mo)

	return result.Error
}

func (u PostServiceImpl) BatchInsert(lmo []model.Post) error {

	result := u.db.CreateInBatches(&lmo, len(lmo))

	log.Printf("Total Rows: %d\n", result.RowsAffected)

	return result.Error
}

func (u PostServiceImpl) GetAllPosts() []model.Post {

	var Posts []model.Post

	result := u.db.Find(&Posts)

	log.Printf("Rows affected %d", result.RowsAffected)

	return Posts
}

func (u PostServiceImpl) GetPostById(id uint) (model.Post, error) {
	var Post model.Post

	result := u.db.Find(&Post, id)

	if result.RowsAffected == 0 {
		return Post, fmt.Errorf("ID %d not found", id)
	}

	return Post, result.Error
}

func (u PostServiceImpl) UpdatePost(mo model.Post) error {
	var Post model.Post

	result := u.db.First(&Post, mo.ID)

	if result.RowsAffected == 0 {
		return fmt.Errorf("ID %d not found on DB", mo.ID)
	}

	mo.CreatedAt = Post.CreatedAt
	mo.State = 2

	result = u.db.Save(&mo)

	if result.RowsAffected == 0 {
		return result.Error
	}

	return result.Error
}

func (u PostServiceImpl) DeletePost(mo model.Post) error {

	result := u.db.Delete(&mo)

	if result.RowsAffected == 0 {
		return fmt.Errorf("ID %d not found", mo.ID)
	}

	return result.Error
}

func PostServiceInit(d *gorm.DB) *PostServiceImpl {
	return &PostServiceImpl{
		db: d,
	}
}
