package service

import (
	"fmt"
	"log"

	"gitlab.com/danielrb/go-blog/model"

	"gorm.io/gorm"
)

type PhotoService interface {
	NewPhoto(mo model.Photo) error
	BatchInsert(lmo []model.Photo) error
	GetAllPhotos() []model.Photo
	GetPhotoById(id uint) (model.Photo, error)
	UpdatePhoto(mo model.Photo) error
	DeletePhoto(mo model.Photo) error
}

type PhotoServiceImpl struct {
	db *gorm.DB
}

func (u PhotoServiceImpl) NewPhoto(mo model.Photo) error {

	result := u.db.Create(&mo)

	return result.Error
}

func (u PhotoServiceImpl) BatchInsert(lmo []model.Photo) error {

	result := u.db.CreateInBatches(&lmo, len(lmo))

	log.Printf("Total Rows: %d\n", result.RowsAffected)

	return result.Error
}

func (u PhotoServiceImpl) GetAllPhotos() []model.Photo {

	var Photos []model.Photo

	result := u.db.Find(&Photos)

	log.Printf("Rows affected %d", result.RowsAffected)

	return Photos
}

func (u PhotoServiceImpl) GetPhotoById(id uint) (model.Photo, error) {
	var Photo model.Photo

	result := u.db.Find(&Photo, id)

	if result.RowsAffected == 0 {
		return Photo, fmt.Errorf("ID %d not found", id)
	}

	return Photo, result.Error
}

func (u PhotoServiceImpl) UpdatePhoto(mo model.Photo) error {
	var Photo model.Photo

	result := u.db.First(&Photo, mo.ID)

	if result.RowsAffected == 0 {
		return fmt.Errorf("ID %d not found on DB", mo.ID)
	}

	mo.CreatedAt = Photo.CreatedAt
	mo.State = 2

	result = u.db.Save(&mo)

	if result.RowsAffected == 0 {
		return result.Error
	}

	return result.Error
}

func (u PhotoServiceImpl) DeletePhoto(mo model.Photo) error {

	result := u.db.Delete(&mo)

	if result.RowsAffected == 0 {
		return fmt.Errorf("ID %d not found", mo.ID)
	}

	return result.Error
}

func PhotoServiceInit(d *gorm.DB) *PhotoServiceImpl {
	return &PhotoServiceImpl{
		db: d,
	}
}
